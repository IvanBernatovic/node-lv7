## Instalacija
- Potrebno je imati instaliranu MongoDB bazu podataka
- Instalarati module sa npm install 
- Pokrenuti aplikaciju sa npm start
- U pregledniku otvoriti http://127.0.0.1:3000

## Upotreba
- Aplikacija pri pokretanju otvara stranicu dobrodošlice na kojoj se moze logirati ili registrirati
- Nakon logiranja vodi vas na index stranicu gdje imate `Add new project`,`Show`, `Edit` ili `Delete`
- Projekti u kojima sudjeluje trenutno prijavljeni korisnik: http://127.0.0.1:3000/projects/my