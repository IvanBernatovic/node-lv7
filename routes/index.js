var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    session = require('express-session'),
    methodOverride = require('method-override'); //used to manipulate POST




/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Projects' });
});

router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

router.get('/register', function(req, res, next) {
  res.render('register', { title: 'Register' });
});

router.get('/logout', function (req, res) {
  session.user = null;
});



router.post('/login', function(req, res, next) {
  var email = req.body.email;
  var password = req.body.password;

  mongoose.model('user').findOne({email:email, password:password}, function (err, user){
    if(err) return next(err);
    if(!user) return res.send('Not logged in!');

    session.user = user;
    return res.redirect("/projects");
  })

});


router.post('/register', function(req, res, next)
{
  var email = req.body.email;
  var password = req.body.password;
  var name = req.body.name;

  var user = {
    email : email,
    password: password,
    name: name,
  };

  mongoose.model('user').create(user, function (err, user){
    if(err) return next(err);

    console.log("User added: " + user);
    session.user = user;
    return res.send('Logged In!');
  })

});






module.exports = router;