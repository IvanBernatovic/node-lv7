var mongoose = require('mongoose');  
var blobSchema = new mongoose.Schema({  
  pr_name: String,
  pr_description: String,
  price: Number,
  members: String,
  startDate: { type: Date, default: Date.now },
  endDate: { type: Date, default: Date.now },
  workDone: Boolean
});
mongoose.model('Blob', blobSchema);